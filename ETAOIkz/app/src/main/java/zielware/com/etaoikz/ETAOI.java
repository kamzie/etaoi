package zielware.com.etaoikz;

import android.content.ClipData;
import android.inputmethodservice.InputMethodService;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;


public class ETAOI extends InputMethodService {

    private List<Button> buttons;

    private static final int[] LETTER_BUTTONS = {
            R.id.ABCDE,
            R.id.FGHIJ,
            R.id.KLMNO,
            R.id.PQRST,
            R.id.UVWYZ,
    };

    private RelativeLayout etaoiLayout;

    private boolean isLetterCapital = true;
    private boolean isNumbers = false;
    private InputConnection ic;

    @Override
    public View onCreateInputView() {
        etaoiLayout = (RelativeLayout)getLayoutInflater().inflate(R.layout.etaoi_layout, null);

        ic = getCurrentInputConnection();
        buttons = new ArrayList<Button>();
        initialize_Letter_Buttons(LETTER_BUTTONS);
        initalizeSpecialLettersButtons();
        initialize_Functional_Buttons();
        setColors();
        setLabels();

        return etaoiLayout;
    }


    private void initialize_Letter_Buttons(int[] letterButtons){
        for (int id : letterButtons){
            Button button = (Button)etaoiLayout.findViewById(id);
            button.setOnTouchListener(new MyTouchListener());
            buttons.add(button);
        }
    }


    private void initializeSpecialLetterButton(int button_id, String special_letter) {
        Button button = ((Button)etaoiLayout.findViewById(button_id));
        button.setOnDragListener(new LetterDragListener(special_letter));
        buttons.add(button);
    }


    private void initalizeSpecialLettersButtons() {
        initializeSpecialLetterButton(R.id.button_space, " ");
        initializeSpecialLetterButton(R.id.button_dot, ".");
        initializeSpecialLetterButton(R.id.button_at, "@");
        initializeSpecialLetterButton(R.id.button_semicolon, ":");
        initializeSpecialLetterButton(R.id.button_slash, "/");
        initializeSpecialLetterButton(R.id.button_x, "x");
    }


    private void initialize_Functional_Buttons(){
        Button button = ((Button)etaoiLayout.findViewById(R.id.button_caps));
        button.setOnDragListener(new MyCaps());
        buttons.add(button);

        button = ((Button)etaoiLayout.findViewById(R.id.button_enter));
        button.setOnDragListener(new MyEnter());
        buttons.add(button);

        button = ((Button)etaoiLayout.findViewById(R.id.button_bck));
        button.setOnDragListener(new MyBackSpace());
        buttons.add(button);

        button = ((Button)etaoiLayout.findViewById(R.id.button_num));
        button.setOnDragListener(new NumLock());
        buttons.add(button);
    }


    public void setColors(){
        buttons.get(0).setBackgroundColor(getResources().getColor(R.color.blue));
        buttons.get(1).setBackgroundColor(getResources().getColor(R.color.green));
        buttons.get(2).setBackgroundColor(getResources().getColor(R.color.yellow));
        buttons.get(3).setBackgroundColor(getResources().getColor(R.color.orange));
        buttons.get(4).setBackgroundColor(getResources().getColor(R.color.red));

        for(int i=5;i<15;i++)
            buttons.get(i).setBackgroundColor(getResources().getColor(R.color.white));

        if(isLetterCapital)
            ((Button)etaoiLayout.findViewById(R.id.button_caps)).setBackgroundColor(getResources().getColor(R.color.silver));

        if (isNumbers)
            ((Button)etaoiLayout.findViewById(R.id.button_num)).setBackgroundColor(getResources().getColor(R.color.silver));
    }


    public void setLabels() {
        if(isNumbers){
            buttons.get(0).setText("01");
            buttons.get(1).setText("23");
            buttons.get(2).setText("45");
            buttons.get(3).setText("67");
            buttons.get(4).setText("89");
        }else {
            if (isLetterCapital) {
                buttons.get(0).setText("ABCDE");
                buttons.get(1).setText("FGHIJ");
                buttons.get(2).setText("KLMNO");
                buttons.get(3).setText("PQRST");
                buttons.get(4).setText("UVWYZ");
            } else {
                buttons.get(0).setText("abcde");
                buttons.get(1).setText("fghij");
                buttons.get(2).setText("klmno");
                buttons.get(3).setText("pqrst");
                buttons.get(4).setText("uvwyz");
            }
        }
    }


    public void setLabelsWhenTouched(View view) {

        String butxt = ((Button) view).getText() + "";

        for(int i = 0; i < 5; i++)
            try {
                buttons.get(i).setText(butxt.substring(i, i + 1));
            } catch (IndexOutOfBoundsException e) {
                buttons.get(i).setText("");
            }
    }


    void setButtonsDragListener(View view){
        for(int i = 0; i < 5; i++)
            buttons.get(i).setOnDragListener(new LetterDragListener(buttons.get(i).getText() + ""));
    }


    void setupDrag(View view){
        ClipData data = ClipData.newPlainText("", "");
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data, shadowBuilder, view, 0); //start dragging the item touched
    }


    public class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                setLabelsWhenTouched(view);
                setButtonsDragListener(view);
                setupDrag(view);
                return true;
            }else{
                return false;
            }
        }
    }


    private View dragOverButton;

    public class LetterDragListener implements View.OnDragListener{
        public String currentLetter = "";


        LetterDragListener(String string){
            currentLetter=string;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    dragOverButton = v;
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    setColors();
                    if (dragOverButton != v)
                        dragOverButton.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DROP:
                    if(this.currentLetter=="x" && isLetterCapital){
                        ic.commitText("X", 1);
                    }
                    else{
                        ic.commitText(currentLetter, 1);
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    setLabels();
                    setColors();
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public class MyBackSpace implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if(ic.getTextBeforeCursor(1,0).toString().length()>0){
                        ic.deleteSurroundingText(1, 0);
                    }

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public class MyEnter implements View.OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public class MyCaps implements View.OnDragListener{

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if ( isLetterCapital ) {
                        isLetterCapital = false;
                    }
                    else {
                        isLetterCapital = true;
                        v.setBackgroundColor(getResources().getColor(R.color.silver));
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    public class NumLock implements View.OnDragListener{

        @Override
        public boolean onDrag(View v, DragEvent event) {

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundColor(getResources().getColor(R.color.silver));
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundColor(getResources().getColor(R.color.white));
                    break;
                case DragEvent.ACTION_DROP:
                    if ( isNumbers ) {
                        isNumbers = false;
                    }
                    else {
                        isNumbers = true;
                        v.setBackgroundColor(getResources().getColor(R.color.silver));
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}